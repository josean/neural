package cn.ms.neural;

import java.util.UUID;

import org.apache.logging.log4j.ThreadContext;
import org.junit.Test;

import cn.ms.neural.common.log.ILog;
import cn.ms.neural.common.log.LogFactory;

public class LogDemo {

	@Test
	public void sysLogTest() {
		ThreadContext.put("aaa", UUID.randomUUID().toString());
		ILog log = LogFactory.getSysBootLog();
		log.debug("This is SysBootLog debug log");
		log.info("This is SysBootLog info log");
		log.warn("This is SysBootLog warn log");
		log.error("This is SysBootLog error log");

		log = LogFactory.getSysDefaultLog();
		log.debug("This is SysDefaultLog debug log");
		log.info("This is SysDefaultLog info log");
		log.warn("This is SysDefaultLog warn log");
		log.error("This is SysDefaultLog error log");

		log = LogFactory.getSysErrorLog();
		log.debug("This is SysErrorLog debug log");
		log.info("This is SysErrorLog info log");
		log.warn("This is SysErrorLog warn log");
		log.error("This is SysErrorLog error log");

		log = LogFactory.getSysMetricsLog();
		log.debug("This is SysMetricsLog debug log");
		log.info("This is SysMetricsLog info log");
		log.warn("This is SysMetricsLog warn log");
		log.error("This is SysMetricsLog error log");

		log = LogFactory.getSysPerfLog();
		log.debug("This is SysPerfLog debug log");
		log.info("This is SysPerfLog info log");
		log.warn("This is SysPerfLog warn log");
		log.error("This is SysPerfLog error log");
	}
	
	@Test
	public void bizLogTest() {
		ILog log = LogFactory.getBizDefaultLog();
		log.debug("This is BizDefaultLog debug log");
		log.info("This is BizDefaultLog info log");
		log.warn("This is BizDefaultLog warn log");
		log.error("This is BizDefaultLog error log");

		log = LogFactory.getBizErrorLog();
		log.debug("This is BizErrorLog debug log");
		log.info("This is BizErrorLog info log");
		log.warn("This is BizErrorLog warn log");
		log.error("This is BizErrorLog error log");

		log = LogFactory.getBizMetricsLog();
		log.debug("This is BizMetricsLog debug log");
		log.info("This is BizMetricsLog info log");
		log.warn("This is BizMetricsLog warn log");
		log.error("This is BizMetricsLog error log");

		log = LogFactory.getBizPerfLog();
		log.debug("This is BizPerfLog debug log");
		log.info("This is BizPerfLog info log");
		log.warn("This is BizPerfLog warn log");
		log.error("This is BizPerfLog error log");
	}

}
