package cn.ms.neural;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import cn.ms.neural.engine.type.ExecuteType;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.Moduler;
import cn.ms.neural.moduler.degrade.IDegrade;
import cn.ms.neural.moduler.degrade.core.DegradeFactory;
import cn.ms.neural.moduler.degrade.handler.IBizDegradeHandler;
import cn.ms.neural.moduler.degrade.handler.IDegradeHandler;
import cn.ms.neural.moduler.degrade.type.DegradeType;
import cn.ms.neural.moduler.degrade.type.StrategyType;
import cn.ms.neural.moduler.flowrate.IFlowrate;
import cn.ms.neural.moduler.flowrate.core.FlowrateFactory;
import cn.ms.neural.moduler.flowrate.entity.FlowrateRule;
import cn.ms.neural.moduler.flowrate.type.FlowrateType;
import cn.ms.neural.moduler.idempotent.Idempotent;
import cn.ms.neural.moduler.idempotent.core.IdempotentFactory;

public class NeuralTest {

	@Test
	public void test() throws Throwable {
		IRoute<String, String> route=new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return "这是route响应报文/"+req;
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return "这是mock响应报文/"+req;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long expend) throws Throwable {
				return 0;
			}
		};
		
		NeuralConf conf=new NeuralConf(0, 10, true);//Call mock after 3 retry
		conf.setNeuralId("key1");
		conf.setDegradeEnable(true);//打开业务降级开关
		conf.setDegradeType(DegradeType.FAULTTOLERANT);//容错降级
		conf.setStrategyType(StrategyType.MOCK);//屏蔽返回MOCK
		
		//conf.setDegradeType(DegradeType.BUSINESS);//屏蔽降级
		IBizDegradeHandler<String, String> bizDegrade=new IBizDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req, IDegradeHandler<String, String> runner) throws Throwable {
				return "这是业务降级响应报文/"+req;
			}
		};
		
		List<FlowrateRule> flowrateRules=new ArrayList<>();
		flowrateRules.add(new FlowrateRule("key1", FlowrateType.CCT_QPS, "MILLISECONDS", 10000, 2, 5, true, 0));
		
		IFlowrate<String, String> flowrate=new FlowrateFactory<>(flowrateRules);
		IDegrade<String, String> degrade=new DegradeFactory<>();
		Idempotent<String, String> dempotent=new IdempotentFactory<>(conf);
		
		Moduler<String, String> moduler=new Moduler<>();
		moduler.setFlowrate(flowrate);
		moduler.setDegrade(degrade);
		moduler.setDempotent(dempotent);
		
		
		String res=new Neural<>(moduler).neural(conf, route, bizDegrade, "测试报文");
		System.out.println("响应报文："+res);
	}
	
}
