package cn.ms.neural.degrade;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import cn.ms.neural.IRoute;
import cn.ms.neural.common.exception.NeuralException;
import cn.ms.neural.common.exception.degrade.DegradeException;
import cn.ms.neural.engine.core.EngineFactory;
import cn.ms.neural.engine.type.ExecuteType;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.degrade.core.DegradeFactory;
import cn.ms.neural.moduler.degrade.handler.IBizDegradeHandler;
import cn.ms.neural.moduler.degrade.handler.IDegradeHandler;
import cn.ms.neural.moduler.degrade.type.DegradeType;
import cn.ms.neural.moduler.degrade.type.StrategyType;

/**
 * 服务降级测试
 * 
 * @author lry
 */
public class DegradeTest {

	String reqRoute="这是请求报文信息";
	final String resRoute="这是Route服务的响应报文信息";
	final String resMock="这是Mock服务的响应报文信息";
	final String resBiz="这是业务降级";
	EngineFactory<String, String> neuralEngine=new EngineFactory<String, String>();
	
	/**
	 * 容错直接抛异常服务降级
	 * @throws Exception
	 */
	@Test
	public void degrade2Exception() throws Throwable {
		IBizDegradeHandler<String, String> bizDegrade=new IBizDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req, IDegradeHandler<String, String> runner) throws Throwable {
				return resBiz;
			}
		};
		final IRoute<String, String> route = new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//模拟业务执行
				throw new RuntimeException("模拟路由业务异常");//模拟失败
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(30));//模拟业务执行
				return resMock;
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long timestamp) throws Throwable {
				return 0;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
				System.out.println("失败信息:"+t.getMessage());
				t.printStackTrace();
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
		};
		NeuralConf conf = new NeuralConf(0, 10, true);//不容错,但打开mock开关
		conf.setDegradeEnable(true);//设置异步响应开关
		try {
			new DegradeFactory<String, String>().degrade(conf, resBiz, bizDegrade, new IDegradeHandler<String, String>() {
				public String handler(NeuralConf conf, String req) throws Throwable {
					return neuralEngine.engine(conf, route, req);
				}
				public String mock(NeuralConf degradeConf, String degradeREQ) throws Throwable {
					return null;
				}
				
			});
		} catch (Throwable t) {
			if(t instanceof DegradeException){
				Assert.assertEquals(true, true);
			}else{
				Assert.assertEquals(true, false);
			}
		}
	}
	
	/**
	 * 容错直接直接Mock服务降级
	 * @throws Exception
	 */
	@Test
	public void degrade2Mock() throws Throwable {
		IBizDegradeHandler<String, String> bizDegrade=new IBizDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req, IDegradeHandler<String, String> runner) throws Throwable {
				return resBiz;
			}
		};
		final IRoute<String, String> route = new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//模拟业务执行
				throw new NeuralException("模拟路由业务异常");//模拟失败
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(30));//模拟业务执行
				return resMock;
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long timestamp) throws Throwable {
				return 0;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
		};
		NeuralConf conf = new NeuralConf(0, 10, true);//不容错,但打开mock开关
		conf.setDegradeEnable(true);//设置异步响应开关
		conf.setStrategyType(StrategyType.MOCK);
		String result=new DegradeFactory<String, String>().degrade(conf, reqRoute, bizDegrade, new IDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req) throws Throwable {
				return neuralEngine.engine(conf, route, req);
			}
			public String mock(NeuralConf degradeConf, String degradeREQ) throws Throwable {
				return null;
			}
			
		});
		Assert.assertEquals(result, resMock);
	}
	
	/**
	 * 容错直接直接业务服务降级
	 * @throws Exception
	 */
	@Test
	public void degrade2Biz() throws Throwable {
		IBizDegradeHandler<String, String> bizDegrade=new IBizDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req, IDegradeHandler<String, String> runner) throws Throwable {
				return resBiz;
			}
		};
		final IRoute<String, String> route = new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//模拟业务执行
				throw new NeuralException("模拟路由业务异常");//模拟失败
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(30));//模拟业务执行
				return resMock;
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long timestamp) throws Throwable {
				return 0;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
		};
		NeuralConf conf = new NeuralConf(0, 10, true);//不容错,但打开mock开关
		conf.setDegradeEnable(true);//设置异步响应开关
		conf.setDegradeType(DegradeType.BUSINESS);
		conf.setStrategyType(StrategyType.MOCK);
		String result=new DegradeFactory<String, String>().degrade(conf, reqRoute, bizDegrade, new IDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req) throws Throwable {
				return neuralEngine.engine(conf, route, req);
			}
			public String mock(NeuralConf degradeConf, String degradeREQ) throws Throwable {
				return null;
			}
			
		});
		Assert.assertEquals(result, resBiz);
	}
	
	/**
	 * 容错直接直接Mock服务降级
	 * @throws Exception
	 */
	@Test
	public void degrade2NULL() throws Throwable {
		IBizDegradeHandler<String, String> bizDegrade=new IBizDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req, IDegradeHandler<String, String> runner) throws Throwable {
				return resBiz;
			}
		};
		final IRoute<String, String> route = new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//模拟业务执行
				throw new NeuralException("模拟路由业务异常");//模拟失败
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(30));//模拟业务执行
				return resMock;
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long timestamp) throws Throwable {
				return 0;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
		};
		NeuralConf conf = new NeuralConf(0, 10, true);//不容错,但打开mock开关
		conf.setDegradeEnable(true);//设置异步响应开关
		conf.setStrategyType(StrategyType.NULL);
		String result=new DegradeFactory<String, String>().degrade(conf, reqRoute, bizDegrade, new IDegradeHandler<String, String>() {
			public String handler(NeuralConf conf, String req) throws Throwable {
				return neuralEngine.engine(conf, route, req);
			}
			public String mock(NeuralConf degradeConf, String degradeREQ) throws Throwable {
				return null;
			}
			
		});
		Assert.assertEquals(result, null);
	}
	
}
