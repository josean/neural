package cn.ms.neural;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import cn.ms.neural.engine.NeuralEngine;
import cn.ms.neural.engine.core.EngineFactory;
import cn.ms.neural.engine.type.ExecuteType;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.Moduler;
import cn.ms.neural.moduler.degrade.IDegrade;
import cn.ms.neural.moduler.degrade.core.DegradeFactory;
import cn.ms.neural.moduler.flowrate.IFlowrate;
import cn.ms.neural.moduler.flowrate.core.FlowrateFactory;
import cn.ms.neural.moduler.flowrate.entity.FlowrateRule;
import cn.ms.neural.moduler.flowrate.type.FlowrateType;
import cn.ms.neural.moduler.idempotent.Idempotent;
import cn.ms.neural.moduler.idempotent.core.IdempotentFactory;

public class NeuralEngineTest {

	public String req="这是请求报文";
	public String mock="这是MOCK报文";
	public String res="这是响应报文";
	
	@Test
	public void test01() {
		NeuralConf conf=new NeuralConf(0, 10, true);//Call mock after 3 retry
		IRoute<String, String> route=new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				throw new RuntimeException();
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return mock;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long expend) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return 0;
			}
		};
		
		NeuralEngine<String, String> neural=new NeuralEngine<String, String>(conf, route, req);
		String resStr=neural.execute();
		Assert.assertEquals(resStr, mock);
		
		System.out.println("调用链("+neural.getCallChain().size()+"):"+neural.getCallChain());
		
	}
	
	/**
	 * 单次请求校验并发和QPS
	 */
	@Test
	public void test02() throws Throwable {
		IRoute<String, String> route=new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return res;
			}
			public String mock(String neuralId, String req) throws Throwable {
				return mock;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long expend) throws Throwable {
				return 0;
			}
		};
		
		List<FlowrateRule> flowrateRules=new ArrayList<FlowrateRule>();
		//运行10个并发,等待0ms,每秒钟允许提交10000个任务,在2ms内稳定增长速率
		flowrateRules.add(new FlowrateRule("key1", FlowrateType.CCT_QPS, "MILLISECONDS", 10000, 2, 5, true, 0));
		
		NeuralConf conf=new NeuralConf(0, 10, true);//Call mock after 3 retry
		conf.setNeuralId("key1");
		conf.setDegradeEnable(false);
		
		IFlowrate<String, String> flowrate=new FlowrateFactory<>(flowrateRules);
		IDegrade<String, String> degrade=new DegradeFactory<>();
		Idempotent<String, String> dempotent=new IdempotentFactory<>(conf);
		
		Moduler<String, String> moduler=new Moduler<String, String>();
		moduler.setFlowrate(flowrate);
		moduler.setDegrade(degrade);
		moduler.setDempotent(dempotent);
		
		Neural<String, String> neural =new Neural<>(moduler);
		String resStr=neural.neural(conf, route, null, req);
		System.out.println("响应结果:"+resStr);
	}
	
	@Test
	public void test03() {
		NeuralConf conf=new NeuralConf(3, 10, true);//Call mock after 3 retry
		conf.setCallbackEnable(true);
		IRoute<String, String> route=new IRoute<String, String>() {
			public String route(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				throw new RuntimeException();
				//return res;
			}
			public String mock(String neuralId, String req) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return mock;
			}
			public void failNotify(String neuralId, ExecuteType execType, String req, Throwable t) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
			}
			public void callback(String neuralId, ExecuteType execType, String res) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
			}
			public long breathCycle(String neuralId, int times, int maxRetryNum, long cycle, long expend) throws Throwable {
				Thread.sleep(new Random().nextInt(50));//Simulation service
				return 0;
			}
		};
		EngineFactory<String, String> neural=new EngineFactory<String, String>();
		String resStr=null;
		try {
			resStr=neural.engine(conf, route, req);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		Assert.assertEquals(resStr, mock);
	}
	
}
