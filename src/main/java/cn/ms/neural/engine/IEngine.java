package cn.ms.neural.engine;

import cn.ms.neural.Adaptor;
import cn.ms.neural.IRoute;
import cn.ms.neural.entity.NeuralConf;

/**
 * 微服务神经元引擎
 * 
 * @author lry
 *
 * @param <REQ>
 * @param <RES>
 */
public interface IEngine<REQ, RES> extends Adaptor {
	
	/**
	 * 神经引擎
	 * 
	 * @param conf
	 * @param route
	 * @param req
	 * @return
	 * @throws Throwable
	 */
	RES engine(NeuralConf conf, IRoute<REQ, RES> route, REQ req) throws Throwable;
	
}
