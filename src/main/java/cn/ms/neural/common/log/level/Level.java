package cn.ms.neural.common.log.level;

/**
 * 日志等级
 * 
 * @author lry
 */
public enum Level{
	
	/**
	 * 'TRACE' log level.
	 */
	TRACE,
	
	/**
	 * 'DEBUG' log level.
	 */
	DEBUG,
	
	/**
	 * 'INFO' log level.
	 */
	INFO,
	
	/**
	 * 'WARN' log level.
	 */
	WARN,
	
	/**
	 * 'ERROR' log level.
	 */
	ERROR;
	
}
