package cn.ms.neural.common.log.mdc;

public interface IMDC<REQ, RES> {

	RES mdc(REQ req) throws Throwable;
	
}
