package cn.ms.neural.common.log.impl;

import cn.ms.neural.common.log.ILog;
import cn.ms.neural.common.log.LogFactory;

/**
 * Log4j2 Log Factory
 * 
 * @author lry
 */
public class Log4j2LogFactory extends LogFactory{
	
	public Log4j2LogFactory() {
		super("Log4j2");
	}

	@Override
	public ILog getLog(String name) {
		return new Log4j2Log(name);
	}

	@Override
	public ILog getLog(Class<?> clazz) {
		return new Log4j2Log(clazz);
	}

}
