package cn.ms.neural.common.log.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

import cn.ms.neural.common.log.ILog;
import cn.ms.neural.common.log.LogFactory;

/**
 * Jdk Log Factory
 * 
 * @author lry
 */
public class JdkLogFactory extends LogFactory{
	
	public JdkLogFactory() {
		super("JDK Logging");
		readConfig();
	}

	@Override
	public ILog getLog(String name) {
		return new JdkLog(name);
	}

	@Override
	public ILog getLog(Class<?> clazz) {
		return new JdkLog(clazz);
	}

	/**
	 * Read the logging.properties configuration file under ClassPath
	 */
	private void readConfig() {
		InputStream in =null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream("logging.properties");
			if(null == in){
				System.err.println("[WARN] Can not find [logging.properties], use [%JRE_HOME%/lib/logging.properties] as default!");
				return;
			}
			LogManager.getLogManager().readConfiguration(in);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				LogManager.getLogManager().readConfiguration();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		} finally {
			if(in!=null){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
