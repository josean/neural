package cn.ms.neural.moduler.flowrate.handler;

import cn.ms.neural.entity.NeuralConf;

public interface IFlowrateHandler<REQ, RES> {
	
	RES handler(NeuralConf flowrateConf, REQ flowrateREQ) throws Throwable;
	
}
