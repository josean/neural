package cn.ms.neural.moduler.flowrate.type;

import cn.ms.neural.type.multitype.ITypeAdaptor;

/**
 * The flow rate type.
 * 
 * @author lry
 */
public enum FlowrateType implements ITypeAdaptor {

	/**
	 * The rate limiter.
	 */
	QPS("QPS", "The rate limiter."),
	
	/**
	 * The concurrent number.
	 */
	CCT("CCT", "The concurrent number."),
	
	/**
	 * The rate limiter and concurrent number.
	 */
	CCT_QPS("CCT_QPS", "The rate limiter and concurrent number.");
	
	String val;
	String msg;
	
	FlowrateType(String val, String msg) {
		this.val=val;
		this.msg=msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val=val;
	}
	
}
