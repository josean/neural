package cn.ms.neural.moduler.flowrate;

import cn.ms.neural.Adaptor;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.flowrate.handler.IFlowrateHandler;

/**
 * 流量控制
 * 
 * @author lry
 */
public interface IFlowrate<REQ, RES> extends Adaptor {

	/**
	 * 流量控制器
	 * <br>
	 * 第一步:并发流控<br>
	 * 第二步:QPS流控<br>
	 * @param flowrateConf
	 * @param flowrateREQ
	 * @param flowrateHandler
	 * @return
	 * @throws Throwable
	 */
	RES flowrate(NeuralConf flowrateConf, REQ flowrateREQ, IFlowrateHandler<REQ, RES> flowrateHandler) throws Throwable;
	
}
