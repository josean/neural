package cn.ms.neural.moduler.reliable.handler;

import cn.ms.neural.moduler.reliable.IReliable;

/**
 * 高可用模块
 * 
 * @author lry
 * @param <REQ>
 * @param <RES>
 */
public class ReliableHandler<REQ, RES> implements IReliable<REQ, RES> {

	@Override
	public void execute() {
	}

	@Override
	public boolean check() {
		return false;
	}

	@Override
	public void reconnect() {
	}

	@Override
	public void mcacherc() {
	}

	@Override
	public void release() {
	}

	@Override
	public void rhandshake() {
	}

	@Override
	public void init() throws Throwable {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destory() throws Throwable {
		// TODO Auto-generated method stub
		
	}

}
