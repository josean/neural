package cn.ms.neural.moduler.degrade.handler;

import cn.ms.neural.entity.NeuralConf;

/**
 * 服务降级处理器
 * 
 * @author lry
 *
 * @param <REQ>
 * @param <RES>
 */
public interface IDegradeHandler<REQ, RES> {

	/**
	 * 服务降级处理中心
	 * 
	 * @param degradeConf
	 * @param degradeREQ
	 * @return
	 * @throws Throwable
	 */
	RES handler(NeuralConf degradeConf, REQ degradeREQ) throws Throwable;

	/**
	 * 服务降级MOCK
	 * 
	 * @param degradeConf
	 * @param degradeREQ
	 * @return
	 * @throws Throwable
	 */
	RES mock(NeuralConf degradeConf, REQ degradeREQ) throws Throwable;

}
