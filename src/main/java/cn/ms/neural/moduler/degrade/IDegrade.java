package cn.ms.neural.moduler.degrade;

import cn.ms.neural.Adaptor;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.degrade.handler.IBizDegradeHandler;
import cn.ms.neural.moduler.degrade.handler.IDegradeHandler;

/**
 * 服务降级
 * <br>
 * 服务降级分类:<br>
 * 1.直接屏蔽降级<br>
 * 2.快速容错降级<br>
 * 3.自定义业务降级
 * <br>
 * 降级策略:<br>
 * 1.返回空<br>
 * 2.抛异常<br>
 * 3.本地mock<br>
 * 4.自定义策略
 * 
 * @author lry
 */
public interface IDegrade<REQ, RES> extends Adaptor {

	/**
	 * 服务降级处理器
	 * 
	 * @param degradeConf
	 * @param degradeREQ
	 * @param bizDegradeHandler
	 * @param degradeHandler
	 * @return
	 * @throws Throwable
	 */
	RES degrade(NeuralConf degradeConf, REQ degradeREQ, IBizDegradeHandler<REQ, RES> bizDegradeHandler, IDegradeHandler<REQ, RES> degradeHandler) throws Throwable;
	
}
