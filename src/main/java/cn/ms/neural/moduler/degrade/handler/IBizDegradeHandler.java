package cn.ms.neural.moduler.degrade.handler;

import cn.ms.neural.entity.NeuralConf;

/**
 * 业务服务降级
 * 
 * @author lry
 */
public interface IBizDegradeHandler<REQ, RES> {

	/**
	 * 业务降级处理器
	 * 
	 * @param bizDegradeConf
	 * @param bizDegradeREQ
	 * @param runner
	 * @return
	 * @throws Throwable
	 */
	RES handler(NeuralConf bizDegradeConf, REQ bizDegradeREQ, IDegradeHandler<REQ, RES> runner) throws Throwable;
	
}
