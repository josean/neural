package cn.ms.neural.moduler.passrate.core;

import java.util.Random;

import cn.ms.neural.common.exception.PassRateException;
import cn.ms.neural.entity.NeuralConf;
import cn.ms.neural.moduler.passrate.IPassRate;

public class PassRateFactory implements IPassRate {

	private NeuralConf conf;
	/**
	 * 放通率计算
	 */
	private  Random passRateRandom=new Random();
	
	public PassRateFactory(NeuralConf conf) {
		this.conf=conf;
	}
	
	public boolean isPassRateEnable() {
		return conf.isPassRateEnable();
	}

	public void passRate() throws Throwable{
		if(!isPassRateEnable()){//开关未开,则跳过校验器
			return;
		}
		if(passRateRandom.nextDouble()>conf.getPassRate()){//放通率控制
			throw new PassRateException("The rate of pass through rate is " + "declined, the current rate(passRate) is "+(conf.getPassRate()*100)+"%.");
		}
	}

}
