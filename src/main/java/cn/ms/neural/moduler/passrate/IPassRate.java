package cn.ms.neural.moduler.passrate;

public interface IPassRate {

	/**
	 * 放通率开关校验
	 * 
	 * @return
	 */
	boolean isPassRateEnable();
	
	/**
	 * 放通率控制
	 * 
	 * @throws Throwable
	 */
	void passRate() throws Throwable;
	
}
