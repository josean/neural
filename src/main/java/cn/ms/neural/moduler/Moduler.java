package cn.ms.neural.moduler;

import cn.ms.neural.engine.IEngine;
import cn.ms.neural.moduler.degrade.IDegrade;
import cn.ms.neural.moduler.flowrate.IFlowrate;
import cn.ms.neural.moduler.idempotent.Idempotent;
import cn.ms.neural.moduler.reliable.IReliable;
import cn.ms.neural.moduler.slow.ISlow;

/**
 * The Moduler Entity.
 * 
 * @author lry
 *
 * @param <REQ>
 * @param <RES>
 */
public class Moduler<REQ, RES> {

	// $NON-NLS-This is core moduler.$
	/**
	 * The flowrate module
	 */
	private IFlowrate<REQ, RES> flowrate;
	/**
	 * The degrade module
	 */
	private IDegrade<REQ, RES> degrade;
	/**
	 * The dempotent module
	 */
	private Idempotent<REQ, RES> dempotent;
	/**
	 * The engine module
	 */
	private IEngine<REQ, RES> engine;
	/**
	 * The reliable module
	 */
	private IReliable<REQ, RES> reliable;
	/**
	 * The slow module
	 */
	private ISlow<REQ, RES> slow;
	
	
	public IDegrade<REQ, RES> getDegrade() {
		return degrade;
	}
	public void setDegrade(IDegrade<REQ, RES> degrade) {
		this.degrade = degrade;
	}
	public IEngine<REQ, RES> getEngine() {
		return engine;
	}
	public void setEngine(IEngine<REQ, RES> engine) {
		this.engine = engine;
	}
	public IFlowrate<REQ, RES> getFlowrate() {
		return flowrate;
	}
	public void setFlowrate(IFlowrate<REQ, RES> flowrate) {
		this.flowrate = flowrate;
	}
	public Idempotent<REQ, RES> getDempotent() {
		return dempotent;
	}
	public void setDempotent(Idempotent<REQ, RES> dempotent) {
		this.dempotent = dempotent;
	}
	public IReliable<REQ, RES> getReliable() {
		return reliable;
	}
	public void setReliable(IReliable<REQ, RES> reliable) {
		this.reliable = reliable;
	}
	public ISlow<REQ, RES> getSlow() {
		return slow;
	}
	public void setSlow(ISlow<REQ, RES> slow) {
		this.slow = slow;
	}
	
}
