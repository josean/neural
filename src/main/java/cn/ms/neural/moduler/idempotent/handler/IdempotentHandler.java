package cn.ms.neural.moduler.idempotent.handler;

import cn.ms.neural.IRoute;
import cn.ms.neural.entity.NeuralConf;

public interface IdempotentHandler<REQ,RES> {

	RES handler(NeuralConf idempotentConf, IRoute<REQ, RES> idempotentRoute, REQ idempotentREQ) throws Throwable;
	
}
