package cn.ms.neural.moduler;

public interface IModuler<REQ, RES> {
	
	/**
	 * Module injection
	 * 
	 * @param moduler Injection Module
	 * @param args Other parameters
	 * @throws Throwable
	 */
	void moduler(Moduler<REQ, RES> moduler,Object...args) throws Throwable;
	
}
